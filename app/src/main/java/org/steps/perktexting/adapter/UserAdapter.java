package org.steps.perktexting.adapter;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.steps.perktexting.PT;
import org.steps.perktexting.R;
import org.steps.perktexting.model.Conversation;
import org.steps.perktexting.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> implements TextToSpeech.OnInitListener {

    private final List<User> mValues;
    String text = null;
    Context ctx;
    private TextToSpeech tts;
    String sender_name = null;
    String receiver_name = null;

    public UserAdapter(Context ctx, List<User> items) {
        mValues = items;
        this.ctx = ctx;
        tts = new TextToSpeech(ctx, this);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_activeuser, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mName.setText(mValues.get(position).getName());
        receiver_name = mValues.get(position).getName();

        char first_char = mValues.get(position).getName().charAt(0);
        holder.mFirstLetter.setText(String.valueOf(first_char));

        PT.getDatabaseRef().child("users").child(PT.getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User users = dataSnapshot.getValue(User.class);

                sender_name = users.getName();
                Log.e("USER",users.getName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                text = "Creating conversation please wait";
                Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                PT.getDatabaseRef().child("conversations").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boolean flag = true;
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Conversation c = snapshot.getValue(Conversation.class);

                            if (c != null && (c.getSenderId().equals(PT.getUserId()) || c.getReceiverId().equals(PT.getUserId()))) {
                                flag = false;
                                break;
                            }


                        }

                        if (flag) {
                            String key = PT.getDatabaseRef().child("conversations").push().getKey();
                            Conversation c = new Conversation(key, sender_name, receiver_name, "New Conversation", PT.getUserId(), holder.mItem.getId());
                            Map<String, Object> con = c.toMap();

                            Map<String, Object> childUpdates = new HashMap<>();
                            childUpdates.put("/conversations/" + key, con);
                            PT.getDatabaseRef().updateChildren(childUpdates);
                            text = "Conversation created successfully with " + holder.mItem.getName() + "Please go to conversation tab to get start";
//                            Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();

                            Toast.makeText(ctx, receiver_name, Toast.LENGTH_SHORT).show();
                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

                        } else {
                            text = "Conversation already exist with " + holder.mItem.getName() + "Please go to conversation tab to get start";
                            //Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
                            Toast.makeText(ctx, sender_name, Toast.LENGTH_SHORT).show();
                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                        // ...
                    }
                });


            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                //   btnSpeak.setEnabled(true);
                //speakOut();
            }

        } else {
            Log.e("TTS", "Initialization Failed!");
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.firstLetter)
        public TextView mFirstLetter;
        @BindView(R.id.name)
        public TextView mName;
        public User mItem;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }
}
