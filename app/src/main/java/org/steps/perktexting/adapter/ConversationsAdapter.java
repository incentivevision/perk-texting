package org.steps.perktexting.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.steps.perktexting.PT;
import org.steps.perktexting.R;
import org.steps.perktexting.fragment.ConversationsFragment.OnListFragmentInteractionListener;
import org.steps.perktexting.model.Conversation;
import org.steps.perktexting.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {

    private final List<Conversation> mValues;
    private final OnListFragmentInteractionListener mListener;
    String name;

    public ConversationsAdapter(List<Conversation> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_conversations, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        if (PT.getUserId().equals(mValues.get(position).getReceiverId())){
            name = mValues.get(position).getSenderName();
            Log.e("Test1", mValues.get(position).getSenderName());
        }
        else {
            name = mValues.get(position).getReceiverName();
            Log.e("Test2", mValues.get(position).getReceiverName());
        }

        Log.e("Test", name);
        holder.mName.setText(name);
        holder.mMessage.setText(mValues.get(position).getMessage());

//        char first_char = name.charAt(0);
   //     holder.mFirstLetter.setText(String.valueOf(first_char));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.firstLetter)
        public TextView mFirstLetter;

        @BindView(R.id.name)
        public TextView mName;

        @BindView(R.id.message)
        public TextView mMessage;

        public Conversation mItem;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
