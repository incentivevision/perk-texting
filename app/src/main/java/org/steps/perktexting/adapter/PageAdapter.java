package org.steps.perktexting.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.steps.perktexting.fragment.ActiveUserFragment;
import org.steps.perktexting.fragment.ContactsFragment;
import org.steps.perktexting.fragment.ConversationsFragment;
import org.steps.perktexting.fragment.ProfileFragment;

/**
 * Created by hussnain on 2/7/18.
 */

public class PageAdapter extends FragmentStatePagerAdapter {
    ActiveUserFragment activeUserFragment;
    ConversationsFragment conversationsFragment;
    ProfileFragment profileFragment;
    ContactsFragment contactsFragment;
    private int mNumOfTabs;


    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                conversationsFragment = new ConversationsFragment();
                return conversationsFragment;
            case 1:
                activeUserFragment = new ActiveUserFragment();
                return activeUserFragment;
            case 2:
                profileFragment = new ProfileFragment();
                return profileFragment;
            case 3:
                contactsFragment = new ContactsFragment();
                return contactsFragment;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
