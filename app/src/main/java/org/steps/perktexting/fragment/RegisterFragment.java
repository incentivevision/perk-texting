package org.steps.perktexting.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import org.steps.perktexting.PT;
import org.steps.perktexting.R;
import org.steps.perktexting.activity.MainActivity;
import org.steps.perktexting.model.User;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;


public class RegisterFragment extends Fragment implements TextToSpeech.OnInitListener {

    @BindView(R.id.name)
    EditText mName;

    @BindView(R.id.email)
    EditText mEmail;

    @BindView(R.id.password)
    EditText mPassword;

    @BindView(R.id.confirm_password)
    EditText mConfirmPassword;
    String text = null;
    private TextToSpeech tts;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        ButterKnife.bind(this,view);
        tts = new TextToSpeech(getContext(), this);

        return view;
    }

    @OnClick(R.id.rLogin)
    public void AlreadyLoginPressed(){
        text = "Already Login";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

        LoginFragment loginFragment = new LoginFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container,loginFragment);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.btn_signup)
    public void ButtonPressed(){
        text = "Sign up";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

        final String n = mName.getText().toString();
        final String e = mEmail.getText().toString();
        String p = mPassword.getText().toString();
        String cp = mConfirmPassword.getText().toString();

        if (n.isEmpty() || e.isEmpty() || p.isEmpty() || cp.isEmpty()) {
            text = "All fields are required!";
            Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else if (!p.equals(cp)) {
            text = "Password & confirm password must be matched.";
            Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else
            PT.getAuth().createUserWithEmailAndPassword(e, p)
                    .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                text = "Sign up successful now signing in please wait";
                                Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
                                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

                                String key = PT.getUserId();
                                User u = new User(PT.getUserId(), n, e, true);
                                Map<String, Object> user = u.toMap();

                                Map<String, Object> childUpdates = new HashMap<>();
                                childUpdates.put("/users/" + key, user);

                                PT.getDatabaseRef().updateChildren(childUpdates);

                                startActivity(new Intent(getContext(), MainActivity.class));
                                getActivity().finish();

                            } else {
                                // If sign in fails, display a message to the user.
                                text = task.getException().getMessage();
                                Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
                                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

                            }

                            // ...
                        }
                    });


    }

    @OnClick(R.id.name)
    public void namePressed() {
        text = "Enter user name";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @OnClick(R.id.email)
    public void emailPressed() {
        text = "Enter email";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @OnClick(R.id.password)
    public void passPressed() {
        text = "Enter password";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @OnClick(R.id.confirm_password)
    public void cpPressed() {
        text = "Enter confirm password";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }



    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                //   btnSpeak.setEnabled(true);
                //speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

}
