package org.steps.perktexting.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.steps.perktexting.PT;
import org.steps.perktexting.R;
import org.steps.perktexting.activity.MainActivity;
import org.steps.perktexting.model.User;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;


public class LoginFragment extends Fragment implements TextToSpeech.OnInitListener {


    String text = null;
    @BindView(R.id.btn_login)
    Button btnlogin;
    @BindView(R.id.sEmail)
    EditText mEmail;
    @BindView(R.id.sPassword)
    EditText mPassword;
    @BindView(R.id.new_account)
    TextView newAccount;
    private TextToSpeech tts;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        ButterKnife.bind(this,view);

        tts = new TextToSpeech(getContext(), this);

        return view;
    }


    @OnClick(R.id.btn_login)
    public void ButtonPressed(){
        text = "Login";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);


        final String e = mEmail.getText().toString();
        String p = mPassword.getText().toString();

        if (e.isEmpty() || p.isEmpty()) {
            text = "All fields are required!";
            Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else
            PT.getAuth().signInWithEmailAndPassword(e, p)
                    .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithEmail:success");
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                text = "Login successful";
                                Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
                                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

                                PT.getDatabaseRef().child("users").child(PT.getUserId()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        User users = dataSnapshot.getValue(User.class);

                                        users.setActive(true);
                                        Map<String, Object> user = users.toMap();


                                        Map<String, Object> childUpdates = new HashMap<>();
                                        childUpdates.put("/users/" + PT.getUserId(), user);

                                        PT.getDatabaseRef().updateChildren(childUpdates);

                                        startActivity(new Intent(getContext(), MainActivity.class));
                                        getActivity().finish();

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        // Getting Post failed, log a message
                                        Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                                        // ...
                                    }
                                });


                            } else {
                                // If sign in fails, display a message to the user.
                                text = task.getException().getMessage();
                                Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
                                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

                            }

                            // ...
                        }
                    });
    }

    @OnClick(R.id.new_account)
    public void NewAccount(){

        text = "create new account";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

        RegisterFragment registerFragment = new RegisterFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container,registerFragment);
        fragmentTransaction.addToBackStack(null).commit();
    }

    @OnClick(R.id.sEmail)
    public void emailClicked() {
        text = "Enter your email";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @OnClick(R.id.sPassword)
    public void passwordClicked() {
        text = "enter your password";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                //   btnSpeak.setEnabled(true);
                //speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }


}
