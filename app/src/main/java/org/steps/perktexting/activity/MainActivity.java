package org.steps.perktexting.activity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.steps.perktexting.R;
import org.steps.perktexting.adapter.PageAdapter;
import org.steps.perktexting.fragment.ConversationsFragment;
import org.steps.perktexting.model.Conversation;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        android.widget.SearchView.OnQueryTextListener, TextToSpeech.OnInitListener,
        ConversationsFragment.OnListFragmentInteractionListener {

    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    String text;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        tts = new TextToSpeech(this, this);

        setSupportActionBar(toolbar);

        mTabLayout.addTab(mTabLayout.newTab().setText("Conversation"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Active Users"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Profile"));
        mTabLayout.addTab(mTabLayout.newTab().setText("All Users"));

        final PagerAdapter adapter = new PageAdapter(getSupportFragmentManager(),mTabLayout.getTabCount());

        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());

                Log.e("Position", String.valueOf(tab.getPosition()));
                switch (tab.getPosition()) {
                    case 0:
                        text = "Conversation Tab selected";
                        break;
                    case 1:
                        text = "Active Users Tab selected";
                        break;
                    case 2:
                        text = "Profile Tab selected";
                        break;
                    case 3:
                        text = "All users Tab selected";
                        break;
                }

                if (text != null)
                    tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_search) {
            text = "Enter what you want to search";
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                //   btnSpeak.setEnabled(true);
                //speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    @Override
    public void onListFragmentInteraction(Conversation item) {
        Intent i = new Intent(this, LogActivity.class);
        i.putExtra("conversation", item);
        startActivity(i);
    }
}
