package org.steps.perktexting.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.steps.perktexting.PT;
import org.steps.perktexting.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.wangyuwei.particleview.ParticleView;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.mParticleView)
    ParticleView mParticleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        if (PT.isSignedIn()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            mParticleView.startAnim();
            mParticleView.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
                @Override
                public void onAnimationEnd() {
                    Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }



    }
}
