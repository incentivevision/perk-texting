package org.steps.perktexting.activity;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.steps.perktexting.PT;
import org.steps.perktexting.R;
import org.steps.perktexting.model.Conversation;
import org.steps.perktexting.model.Message;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

import static android.content.ContentValues.TAG;

public class LogActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {
    Conversation con;
    int status = 0;

    @BindView(R.id.chat_view)
    ChatView chatView;
    String text;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        ButterKnife.bind(this);
//        status = 1;

        con = (Conversation) getIntent().getSerializableExtra("conversation");
        tts = new TextToSpeech(this, this);
        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {
                text = "Message sent";
                String key = PT.getDatabaseRef().child("messages").child(con.getId()).push().getKey();
                Message m = new Message(key, chatMessage.getMessage(), PT.getUserId());
                Map<String, Object> cm = m.toMap();

                Map<String, Object> childUpdates = new HashMap<>();

                Conversation c = new Conversation(con.getId(), con.getSenderName(),con.getReceiverName(), chatMessage.getMessage(), con.getSenderId(), con.getReceiverId());
                Map<String, Object> cons = c.toMap();


                childUpdates.put("/conversations/" + con.getId(), cons);

                childUpdates.put("/messages/" + con.getId() + "/" + key, cm);
                PT.getDatabaseRef().updateChildren(childUpdates);

                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                return false;
            }
        });


        PT.getDatabaseRef().child("messages").child(con.getId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatView.clearMessages();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Message m = snapshot.getValue(Message.class);

                    if (m != null && !m.getSenderId().equals(PT.getUserId())) {
//                        if (status == 1)
                        tts.speak("New received message is " + m.getMessage(), TextToSpeech.QUEUE_FLUSH, null);
                        chatView.addMessage(new ChatMessage(m.getMessage(), 0, ChatMessage.Type.RECEIVED));
                    } else {
                        tts.speak("Message sent", TextToSpeech.QUEUE_FLUSH, null);
                        chatView.addMessage(new ChatMessage(m.getMessage(), 0, ChatMessage.Type.SENT));
                    }


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        });

    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                //   btnSpeak.setEnabled(true);
                //speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }
}
