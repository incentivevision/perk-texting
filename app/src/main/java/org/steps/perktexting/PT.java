package org.steps.perktexting;

import android.app.Application;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by ranatayyab on 2/7/18.
 */

public class PT extends Application {

    /**
     * @return
     */
    public static FirebaseAuth getAuth() {
        return FirebaseAuth.getInstance();
    }

    /**
     * @return
     */
    public static String getUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    /**
     * @return
     */
    public static Boolean isSignedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    /**
     * @return
     */
    public static DatabaseReference getDatabaseRef() {
        return FirebaseDatabase.getInstance().getReference();
    }
}
