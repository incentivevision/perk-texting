package org.steps.perktexting.model;

import java.util.HashMap;

/**
 * Created by ranatayyab on 2/7/18.
 */

public class User {
    private String id;
    private String name;
    private String email;
    private boolean active;

    public User() {
    }
    public User(String id, String name, String email, boolean active) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("id", this.id);
        map.put("name", this.name);
        map.put("email", this.email);
        map.put("active", this.active);

        return map;
    }
}
