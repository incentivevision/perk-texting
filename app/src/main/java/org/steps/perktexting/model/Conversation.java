package org.steps.perktexting.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.PriorityQueue;

/**
 * Created by ranatayyab on 2/7/18.
 */

public class Conversation implements Serializable {
    private String id;
    private String senderName;
    private String message;
    private String receiverName;
    private String senderId;
    private String receiverId;

    public Conversation() {
    }

    public Conversation(String id, String senderName, String receiverName, String message, String senderId, String receiverId) {
        this.id = id;
        this.senderName = senderName;
        this.receiverName = receiverName;
        this.message = message;
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public String getId() {
        return id;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }


    public String getMessage() {
        return message;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }


    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("id", this.id);
        map.put("senderName", this.senderName);
        map.put("receiverName",receiverName);
        map.put("message", this.message);
        map.put("senderId", this.senderId);
        map.put("receiverId", this.receiverId);

        return map;
    }
}
