package org.steps.perktexting.model;

import java.util.HashMap;

/**
 * Created by ranatayyab on 2/7/18.
 */

public class Message {
    private String id;
    private String message;
    private String senderId;

    public Message() {
    }
    public Message(String id, String message, String senderId) {
        this.id = id;
        this.message = message;
        this.senderId = senderId;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getSenderId() {
        return senderId;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("id", this.id);
        map.put("message", this.message);
        map.put("senderId", this.senderId);

        return map;
    }
}
